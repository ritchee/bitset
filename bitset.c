#include <string.h>
#include <errno.h>
#include <assert.h>

#include "bitset.h"

#define LEN_BYTE BITSET_BYTE
#define LEN_WORD BITSET_WORD

bitset* 
bitset_init(bitset* bset, size_t len)
{
	if (len == 0 || len > BITSET_MAX_LEN)
		return NULL;

	/* align len on 32 or 64 */
	if (len % BITSET_UNIT != 0) 
		len = (len & ~(BITSET_UNIT - 1)) + BITSET_UNIT;

	bset->bvec = BITSET_ALLOC(LEN_BYTE(len));
	if (bset->bvec == NULL)
		return NULL;

	memset(bset->bvec, 0, LEN_BYTE(len));
	bset->len = len;

	return bset;
}

size_t 
bitset_count(bitset* bset)
{
	size_t i, count, len;
	bitset_int iset;

	len = LEN_WORD(bset->len);

	for (i = 0, count = 0; i < len; i++) {
		iset = bset->bvec[i];

		while (iset) {
			count++;
			iset = iset & (iset - 1);
		}
	}

	return count;
}

size_t 
bitset_align(size_t num)
{
#ifdef BITSET_INT32
	return (num & ~(BITSET_UNIT - 1)) + 
		(((num & (BITSET_UNIT - 1)) != 0) << 5);
#else
	return (num & ~(BITSET_UNIT - 1)) + 
		(((num & (BITSET_UNIT - 1)) != 0) << 6);
#endif /* BITSET_INT64 */
}


bitset* 
bitset_extend(bitset* bset)
{
	bitset newbs;

	newbs.len = bset->len * 2;
	if (newbs.len < bset->len) {
		errno = EOVERFLOW;

		return NULL;
	}

	newbs.bvec = BITSET_RALLOC(bset->bvec, LEN_BYTE(newbs.len));
	if (newbs.bvec == NULL)
		return NULL;

	memset(&newbs.bvec[LEN_WORD(bset->len)], 0, LEN_WORD(bset->len));
	memcpy(bset, &newbs, sizeof(bitset));

	return bset;
}

size_t
bitset_to_str(bitset* bset, char* s, size_t len)
{
	size_t i;
	char tmp;
	bitset_int imask, iset;

	if (len == 0)
		return 0;

	for (i = 0, imask = 0; i < len - 1 && i < bset->len; i++) {
		if (imask == 0) {
			imask = 1;
			iset = bset->bvec[LEN_WORD(i)];
		}

		s[i] = (imask & iset) ? '1' : '0';

		imask <<= 1;
	}

	s[i] = '\0';

	len = i;
	for (i = 0; i < len / 2; i++) {
		tmp = s[i];
		s[i] = s[len - i - 1];
		s[len - i - 1] = tmp;
	}

	return len;
}

size_t
bitset_find_unset(bitset* bset)
{
	size_t i, fbit;
	bitset_int iset;

	for (i = 0; i < LEN_WORD(bset->len); i++) {
		iset = bset->bvec[i];

		if (iset != (bitset_int)-1) {
			for (fbit = 0; fbit < BITSET_UNIT; fbit++) {
				if (((bitset_int)(1 << fbit) & iset) == 0)
					return fbit + i * BITSET_UNIT;
			}
		}
	}

	return bset->len;
}

size_t 
bitset_find_set(bitset* bset)
{
	size_t i, fbit;
	bitset_int iset;

	for (i = 0; i < LEN_WORD(bset->len); i++) {
		iset = bset->bvec[i];

		if (iset != 0) {
			for (fbit = 0; fbit < BITSET_UNIT; fbit++) {
				if ((bitset_int)(1 << fbit) & iset)
					return fbit + i * BITSET_UNIT;
			}
		}
	}

	return bset->len;

}

int 
bitset_empty(bitset* bset)
{
	size_t i;

	for (i = 0; i < LEN_WORD(bset->len); i++) {
		if (bset->bvec[i] != 0)
			return 0;
	}

	return 1;
}

int 
bitset_is_set(bitset* bset, size_t bit)
{
	bitset_int set;

	set = bset->bvec[LEN_WORD(bit)];

	return set & ((bitset_int)1 << (bit % BITSET_UNIT));
}

void 
bitset_set(bitset* bset, size_t bit)
{
	bitset_int set;

	set = bset->bvec[LEN_WORD(bit)];
	set |= ((bitset_int)1 << (bit % BITSET_UNIT));
	bset->bvec[LEN_WORD(bit)] = set;
}

void 
bitset_unset(bitset* bset, size_t bit)
{
	bitset_int set;

	set = bset->bvec[LEN_WORD(bit)];
	set &= ~((bitset_int)1 << (bit % BITSET_UNIT));
	bset->bvec[LEN_WORD(bit)] = set;
}

void 
bitset_flip(bitset* bset, size_t bit)
{
	bitset_int set, tmp;

	set = bset->bvec[LEN_WORD(bit)];
	tmp = (bitset_int)1 << (bit % BITSET_UNIT);

	set = (set & ~(tmp)) | ((set & (tmp)) ^ (tmp));
	bset->bvec[LEN_WORD(bit)] = set;
}

void 
bitset_clear(bitset* bset)
{
	BITSET_FREE(bset->bvec);
}

