#ifndef _BITSET_H_
#define _BITSET_H_

#include <stdlib.h>
#include <stdint.h>

#define BITSET_MAX_LEN 65536 /* set upper bound if big int is a concern */
#define BITSET_MIN_LEN (sizeof(bitset_int) * 8)
#define BITSET_UNIT BITSET_MIN_LEN
#define BITSET_ALLOC malloc
#define BITSET_RALLOC realloc
#define BITSET_FREE free

/* 64-bit int is used by default */
#ifdef BITSET_INT32
typedef uint32_t bitset_int;
#else
typedef uint64_t bitset_int;
#endif /* BITSET_LONG */

typedef struct {
	size_t len;
	bitset_int* bvec;
	/**
	 * bitset_int bvec[]; to save more memory but bitset is less decoupled
	 */
} bitset;

/* converts bit to other units, 8 bits = 1 byte, 64(or 32) bits = 1 word */
#define BITSET_BYTE(n) ((n) / 8)            
#define BITSET_WORD(n) ((n) / BITSET_UNIT)

/**
 * \brief initialize a bitset
 *
 * \param bset uninitialized bitset
 * \param len number of bit
 *
 * \return pointer to initialized bitset
 */
bitset* bitset_init(bitset* bset, size_t len);
size_t bitset_count(bitset* bset);
size_t bitset_align(size_t num);
bitset* bitset_extend(bitset* bset);
size_t bitset_to_str(bitset* bset, char* s, size_t len);
size_t bitset_find_unset(bitset* bset);
size_t bitset_find_set(bitset* bset);
int bitset_empty(bitset* bset);
int bitset_is_set(bitset* bset, size_t bit);
void bitset_set(bitset* bset, size_t bit);
void bitset_unset(bitset* bset, size_t bit);
void bitset_flip(bitset* bset, size_t bit);
void bitset_clear(bitset* bset);

#endif /* _BITSET_H_ */
